from django import forms
from django.db import models
from datetime import date, timedelta
from django.urls import reverse
from django.db.models import Sum
from compositefk.fields import (
    CompositeForeignKey,
    RawFieldValue,
    LocalFieldValue,
    CompositeOneToOneField,
    FunctionBasedFieldValue,
)
from django.core.validators import MaxValueValidator, MinValueValidator
import datetime
import string, random

# Create your models here.
class Department(models.Model):
	department_name = models.CharField(primary_key=True, max_length = 50)
	peso_multiplier = models.PositiveSmallIntegerField(default = 0)

	def get_absolute_url(self):
		return reverse('payout:departments')

	def __str__(self):
		return self.department_name

class Worker(models.Model):
	worker_name = models.CharField(max_length = 250)
	department_name = models.ForeignKey(Department, on_delete = models.DO_NOTHING, default = "None")
	accumulated_tickets = models.BigIntegerField( default = "0")
	accumulated_salary = models.FloatField( default = "0")
	last_date_paid = models.DateField(default = "1000-01-01")
	worker_type = models.CharField(max_length = 3, default = "REG")

	def get_absolute_url(self):
		return reverse('payout:workers')

	def __str__(self):
		# return str(self.pk) + ' ' + self.worker_name
		return self.worker_name

class Advanced_Payment(models.Model):
	worker_id = models.ForeignKey(Worker, on_delete = models.CASCADE)
	statuschoices = (("Pending", "Pending"), ("Done", "Done"))
	payment_status = models.CharField(max_length=10, choices = statuschoices, default = "Pending")
	date = models.DateField(default = "1000-01-01")

	class Meta:
		verbose_name ='Advanced Payment'
		verbose_name_plural = 'Advanced Payments'

	def get_absolute_url(self):
		return reverse('payout:advanced')

	def __str__(self):
		# return str(self.pk) + ' ' + self.worker_name
		return str(self.pk) + ' ' + str(self.worker_id)

class Payout(models.Model):
	date = models.DateField(default = "1000-01-01")
	number_of_tickets = models.PositiveSmallIntegerField(default = 0)
	amount_of_bonus = models.FloatField(default = 0.00)
	total_salary = models.FloatField(default = 0.00)
	worker = models.ForeignKey(Worker, on_delete = models.CASCADE)


	def save(self, *args, **kwargs):
		today = datetime.date.today()
		idx = (today.weekday()+1) % 7
		sun = today - datetime.timedelta(idx)
		sat = today + datetime.timedelta(6 - idx)

		self.date = sat
		self.total_salary = self.amount_of_bonus +(self.number_of_tickets*self.worker.department_name.peso_multiplier)#here is an error
		Worker.objects.filter(id = self.worker.id).update(last_date_paid = sat, accumulated_salary =+ self.total_salary, accumulated_tickets =+ self.number_of_tickets)

		super(Payout, self).save(*args, **kwargs)
		
		# if datetime.datetime.now() == sat:
		# 	if sat.hour() == 23:

		currweeklyreport = Weekly_Report.objects.filter(date=sat)
		if currweeklyreport.exists():
			print('Report already exists')
			print(sun)
			payouts = Payout.objects.filter(date__range=[sun,sat]).aggregate(Sum('total_salary'))
			# total = payouts
			print(payouts)
			total = payouts['total_salary__sum']
			# for payout in payouts:
			# 	total =+ payout['total_salary']
			currweeklyreport.update(weekly_total=total)
		else: 
			Weekly_Report().save()

	def get_absolute_url(self):
		return reverse('payout:report')

	def __str__(self):
		return str(self.pk)

class Weekly_Report(models.Model):
	date = models.DateField(default = "1000-01-01", primary_key = True)
	weekly_total = models.FloatField() 

	class Meta:
		verbose_name ='Weekly Report'
		verbose_name_plural = 'Weekly Reports'


	# if self.objects.filter(date=sat).exists():
	# if datetime.datetime.now() == sat:
	# 	if sat.hour() == 23:
	# 		print('Report already exists')
	# 	else: 
	# 		Weekly_Report.save()

	def save(self, *args, **kwargs):
		today = datetime.datetime.today()
		idx = (today.weekday()+1) % 7
		sun = today - datetime.timedelta(idx)
		sat = today + datetime.timedelta(6 - idx)
		self.date = sat
		payouts = Payout.objects.filter(date__range=[sun,sat]).values('total_salary')

		total = 0
		for payout in payouts:
			total =+ payout['total_salary']

		self.weekly_total = total

		super(Weekly_Report, self).save(*args, **kwargs)

	def __str__(self):
		return 'Week of ' + str(self.pk)

class Ticket(models.Model):
	style_name = models.CharField(default='style name', max_length=50)
	color = models.CharField(default='color', max_length=20)
	size1 = models.PositiveSmallIntegerField(verbose_name='35', default=0, validators=[MaxValueValidator(20), MinValueValidator(0)])
	size2= models.PositiveSmallIntegerField(verbose_name='36', default=0, validators=[MaxValueValidator(20), MinValueValidator(0)])
	size3 = models.PositiveSmallIntegerField(verbose_name='37', default=0, validators=[MaxValueValidator(20), MinValueValidator(0)])
	size4 = models.PositiveSmallIntegerField(verbose_name='38', default=0, validators=[MaxValueValidator(20), MinValueValidator(0)])
	size5 = models.PositiveSmallIntegerField(verbose_name='39', default=0, validators=[MaxValueValidator(20), MinValueValidator(0)])
	size6 = models.PositiveSmallIntegerField(verbose_name='40', default=0, validators=[MaxValueValidator(20), MinValueValidator(0)])

	no_pairs = models.PositiveSmallIntegerField(default=0, validators=[MaxValueValidator(20)])

	class Meta(object):
		unique_together = [("style_name","color")]

	def get_absolute_url(self):
		return reverse('payout:ticketsummary')

	def clean(self):
		if self.no_pairs > 20:
			raise form.ValidationError("Total number of pairs is above 20. You currently have " + self.no_pairs)

	def save(self, *args, **kwargs):

		self.no_pairs = self.size1 + self.size2 + self.size3 +  self.size4 + self.size5 + self.size6

		# self.full_clean()

		super(Ticket, self).save(*args, **kwargs)