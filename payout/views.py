from django.shortcuts import render, get_object_or_404
from django.http import Http404
from .models import Department, Worker, Payout, Weekly_Report, Advanced_Payment, Ticket
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, FormView
from .forms import PayoutForm, ReportForm
from datetime import date, timedelta
from django import forms
from django.core.exceptions import ValidationError
import datetime
# Create your views here.
def index(request):
	return render(request, 'payout/index.html')

def departments(request):
	departments = Department.objects.all()
	context = {
		'departments' : departments,
	}
	return render(request, 'payout/departments.html', context)

def workers(request):
	workers = Worker.objects.all()
	context = {
		'workers' : workers,
	}
	return render(request, 'payout/workers.html', context)

def report(request):

	today = datetime.date.today()
	idx = (today.weekday()+1) % 7
	sun = today - datetime.timedelta(idx)
	sat = today + datetime.timedelta(6 - idx)

	payouts = Payout.objects.filter(date=sat)

	context = {
		'payouts' : payouts,
		'sat': sat,
	}
	return render(request, 'payout/report.html', context)

def weeklyreports(request):

	reports = Weekly_Report.objects.all()
	context = {
		'reports': reports,
	}
	return render(request, 'payout/weeklyreports.html', context)

def advancedpayments(request):
	payments = Advanced_Payment.objects.filter(payment_status="Pending")
	context = {
		'payments' : payments,
	}
	return render(request, 'payout/advancedpayments.html', context)

def tickets(request):
	tickets = Ticket.objects.all()
	context = {
		'tickets': tickets,
	}
	return render(request, 'payout/tickets.html', context)

def workername_present(worker_name):
	if Worker.objects.filter(worker_name=worker_name).exists():
		return True
	return False

def ticketsummary(request):
	session_stylename = request.session.get('style_name')
	session_color = request.session.get('color')
	tickets = Ticket.objects.filter(style_name=session_stylename, color=session_color)
	# print (ticket)
	departments = Department.objects.all()
	context = {
		'tickets': tickets,
		'departments': departments,
	}
	return render(request, 'payout/ticketsummary.html', context)

def ticket(request, pk):
	# tickets = Ticket.objects.get(pk=pk)
	departments = Department.objects.all()
	ticket = get_object_or_404(Ticket, pk=pk)
	print (ticket)

	context = {
		'ticket' : ticket,
		'departments': departments,
	}
	return render(request, 'payout/ticket.html', context)

class WorkerCreate(CreateView):
	model = Worker
	fields = ['worker_name', 'department_name']

	def form_valid(self, form):
		if workername_present(self.request.POST.get('worker_name')) == True:
			form.add_error(None, ValidationError("Worker name is already taken"))
			return super().form_invalid(form)
		return super().form_valid(form)

class WorkerUpdate(UpdateView):
	model = Worker
	fields = ['worker_name','department_name']
	template_name='payout/update_worker.html'

class PayoutCreate(CreateView):
	model = Payout
	fields = ['worker', 'number_of_tickets', 'amount_of_bonus']

class PayoutUpdate(UpdateView):
	model = Payout
	fields = ['number_of_tickets','amount_of_bonus']
	template_name = 'payout/update_payout.html'

class DepartmentUpdate(UpdateView):
	model = Department
	fields = ['peso_multiplier']
	template_name = 'payout/update_pesomultiplier.html'

class Advanced_PaymentCreate(CreateView):
	model = Advanced_Payment
	fields = ['worker_id']
	template_name = 'payout/create_advanced.html'

class Advanced_PaymentUpdate(UpdateView):
	model = Advanced_Payment
	fields = ['payment_status']
	template_name = 'payout/update_advanced.html'

class TicketCreate(CreateView):
	model = Ticket
	fields = ['style_name','color', 'size1', 'size2', 'size3', 'size4', 'size5', 'size6']
	template_name = 'payout/create_ticket.html'

	def form_valid(self, form):
		# ticket = get_object_or_404(Ticket, id=self.kwargs['ticket'])
		# form.instance.no_pairs = ticket.no_pairs
		# self.object = self.get_object()
		size1 = int(self.request.POST.get('size1'))
		size2 = int(self.request.POST.get('size2'))
		size3 = int(self.request.POST.get('size3'))
		size4 = int(self.request.POST.get('size4'))
		size5 = int(self.request.POST.get('size5'))
		size6 = int(self.request.POST.get('size6'))
		total = size1 + size2 + size3 + size4 + size5 + size6

		if total > 20:
			form.add_error(None, ValidationError("Total number of pairs is above 20. You currently have " + str(total) +"."))
			return super().form_invalid(form)

		style_name_session = self.request.POST.get('style_name')
		color_session = self.request.POST.get('color')

		self.request.session['style_name'] = style_name_session
		self.request.session['color'] = color_session

		return super().form_valid(form)

class TicketUpdate(UpdateView):
	model = Ticket
	fields = ['style_name','color', 'size1', 'size2', 'size3', 'size4', 'size5', 'size6']
	template_name = 'payout/update_ticket.html'

	def form_valid(self, form):
		# ticket = get_object_or_404(Ticket, id=self.kwargs['ticket'])
		# form.instance.no_pairs = ticket.no_pairs
		# self.object = self.get_object()
		size1 = int(self.request.POST.get('size1'))
		size2 = int(self.request.POST.get('size2'))
		size3 = int(self.request.POST.get('size3'))
		size4 = int(self.request.POST.get('size4'))
		size5 = int(self.request.POST.get('size5'))
		size6 = int(self.request.POST.get('size6'))
		total = size1 + size2 + size3 + size4 + size5 + size6

		if total > 20:
			form.add_error(None, ValidationError("Total number of pairs is above 20. You currently have " + str(total) +"."))
			return super().form_invalid(form)

		style_name_session = self.request.POST.get('style_name')
		color_session = self.request.POST.get('color')

		self.request.session['style_name'] = style_name_session
		self.request.session['color'] = color_session

		return super().form_valid(form)


# def worker(request, worker_id):
# 	# worker = Worker.objects.get(pk=worker_id)
# 	worker = get_object_or_404(Worker, pk=worker_id)
# 	context = {
# 		'worker' : worker,
# 	}
# 	return render(request, 'payout/worker.html', context)

# class PayoutView(TemplateView):
# 	template_name = 'payout/payout_form.html'

# 	def get(self, request, *args, **kwargs):
# 	    payout_form = PayoutForm(self.request.GET or None)
# 	    report_form = ReportForm(self.request.GET or None)
# 	    context = self.get_context_data(**kwargs)
# 	    context['payout_form'] = payout_form
# 	    context['question_form'] = report_form
# 	    context['worker'] = Worker.objects.all()
# 	    context['payouts'] = Payout.objects.all()
# 	    return self.render_to_response(context)

# class PayoutFormView(FormView):
#     form_class = PayoutForm
#     template_name = 'payout/payout_form.html'
#     success_url = 'payout/report.html'

#     def post(self, request, *args, **kwargs):
# 	    payout_form = self.form_class(request.POST)
# 	    report_form = ReportForm()
# 	    if payout_form.is_valid():
# 	        payout_form.save()
# 	        return self.render_to_response(
# 	            self.get_context_data(
# 	            success=True
# 	        )
# 	    )
# 	    else:
# 	    	print ('not saved')
# 	    	return self.render_to_response(
# 		    	self.get_context_data(
# 		            payout_form=payout_form,
# 		        )
# 		    )

# class ReportFormView(FormView):
#     form_class = ReportForm
#     template_name = 'payout/payout_form.html'
#     success_url = 'payout/payout_form.html'

#     def post(self, request, *args, **kwargs):
#         report_form = self.form_class(request.POST)
#         payout_form = PayoutForm()
#         if report_form.is_valid():
#             report_form.save()
#             return self.render_to_response(
#                 self.get_context_data(
#                 success=True
#             )
#         )
#         else:
#             return self.render_to_response(
#             self.get_context_data(
#                     report_form=report_form,
#                     payout_form=payout_form
#             )
#         )