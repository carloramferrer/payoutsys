from django.urls import path
from . import views

app_name = 'payout'

urlpatterns = [
	path('', views.index, name='index'),
	path('departments', views.departments, name='departments'),
	path('department/<pk>', views.DepartmentUpdate.as_view(), name='updatedepartment'),
	path('workers', views.workers, name ='workers'),
	path('addworker', views.WorkerCreate.as_view(), name = 'addworker'),
	path('worker/<pk>', views.WorkerUpdate.as_view(), name ='updateworker'),
	path('newpayout', views.PayoutCreate.as_view(), name='newpayout'),
	path('report', views.report, name='report'),
	path('payout/<pk>', views.PayoutUpdate.as_view(), name = 'updatepayout'),
	path('weeklyreports', views.weeklyreports, name='weeklyreports'),
	path('advancedpayments', views.advancedpayments, name='advanced'),
	path('addadvancedpayment', views.Advanced_PaymentCreate.as_view(), name = 'addadvanced'),
	path('advancedpayment/<pk>', views.Advanced_PaymentUpdate.as_view(), name = 'updateadvanced'),
	path('tickets', views.tickets, name='tickets'),
	path('addticket', views.TicketCreate.as_view(), name = 'addticket'),
	path('ticketsummary', views.ticketsummary, name='ticketsummary'),
	path('updateticket/<pk>', views.TicketUpdate.as_view(), name = 'updateticket'),
	path('ticket/<pk>', views.ticket, name='viewticket'),
]