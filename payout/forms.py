# from django import forms
# from .models import Payout, Weekly_Report, Worker, Department

# class PayoutForm(forms.Form):
# 	worker = forms.ForeignKey(Worker, on_delete = CASCADE)
# 	number_of_tickets = models.PositiveSmallIntegerField()
# 	amount_of_bonus = models.FloatField()
# 	total_salary = models.FloatField()

from django.forms import ModelForm
from .models import Payout, Weekly_Report


class PayoutForm(ModelForm):
    class Meta:
        model = Payout
        fields = ['worker', 'number_of_tickets', 'amount_of_bonus', 'total_salary']


class ReportForm(ModelForm):
    class Meta:
        model = Weekly_Report
        fields = ['date', 'weekly_total']
        