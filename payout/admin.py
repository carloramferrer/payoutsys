from django.contrib import admin
from .models import Department, Worker, Payout, Weekly_Report, Advanced_Payment, Ticket
# Register your models here.

admin.site.register(Department)
admin.site.register(Worker)
admin.site.register(Payout)
admin.site.register(Weekly_Report)
admin.site.register(Advanced_Payment)
admin.site.register(Ticket)